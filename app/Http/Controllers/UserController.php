<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;

class UserController extends Controller
{
      /**
      * API Register
      *
      * @param Request $request
      * @return \Illuminate\Http\JsonResponse
      */
     public function register(Request $request)
     {
         $credentials = $request->only('first_name', 'last_name', 'email', 'password');

         $rules = [
             'first_name' => 'required|max:255',
             'last_name' => 'required|max:255',
             'email' => 'required|email|max:255|unique:users',
             'password' => 'required|min:8|'
         ];

         $validator = Validator::make($credentials, $rules);

         if($validator->fails()) {
             return response()->json(['success'=> false, 'error'=> $validator->messages()]);
         }

         $first_name = $request->first_name;
         $last_name = $request->last_name;
         $email = $request->email;
         $password = $request->password;

         $user = User::create(['first_name' => $first_name, 'last_name' => $last_name, 'email' => $email, 'password' => Hash::make($password), 'token' => '']);

         return response()->json('Thanks for signing up!');
      }

    /**
     * API Login, on success return JWT Auth token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $validator = Validator::make($credentials, $rules);

        if($validator->fails()) {
            return response()->json(['error'=> $validator->messages()], 401);
        }

        $token = JWTAuth::attempt($credentials);

        $user = DB::table('users')->where('email',$credentials['email'])->update(['token' => $token]);
        $user = DB::table('users')->where('email',$credentials['email'])->first();

        //$credentials['is_verified'] = 1;

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token) {
                return response()->json(['error' => 'Error in user or password'], 401);
            }

        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'Failed to login, please try again.'], 500);
        }
        // all good so return the token
        return response()->json([ 'id' => $user->id, 'first_name' => $user->first_name, 'last_name' => $user->last_name, 'email' => $user->email, 'token' => $token ], 200);
    }

    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request) {

        $this->validate($request, ['token' => 'required']);

        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true, 'message'=> "You have successfully logged out."]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (request()->header('Accept') != 'application/json') {
          return response()->json(['error' => "Request should have header 'Acept' with the value: 'aplication/json'"],403);
      }

        $users = User::get(['first_name', 'last_name', 'email', 'password']);

        return response()->json($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if (request()->header('Accept') != 'application/json') {
          return response()->json(['error' => "Request should have header 'Acept' with the value: 'aplication/json'"],403);
      }

        $obj = [
          'first_name' => $request->input("first_name"),
          'last_name'=> $request->input("last_name"),
          'email' => $request->input("email"),
          'password' => Hash::make($request->input("password")),
          'token' => '',
        ];

        $validator = Validator::make($obj,[
            'first_name' => 'required|max:60',
            'last_name' => 'required|max:60',
            'email' => 'required|email',
            'password' => 'required|min8',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(),201);
        }

        try {
            $user = User::select('first_name', 'last_name', 'email', 'password')
            ->where('email',$obj['email'])
            ->first();

            if ($user == true) {
              return response()->json(['error' => 'The user already exists'],201);
            }

            $user = User::create($obj);

            return response()->json('',204);
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      if (request()->header('Accept') != 'application/json') {
          return response()->json(['error' => "Request should have header 'Acept' with the value: 'aplication/json'"],403);
      }
        $user = User::select('first_name', 'last_name', 'email', 'password')
        ->where('id',$id)
        ->first();

        if ($user == false) {
          return response()->json(['error' => 'Not found user'],201);
        }

        return response()->json($user,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (request()->header('Accept') != 'application/json') {
            return response()->json(['error' => "Request should have header 'Acept' with the value: 'aplication/json'"],403);
        }

        $obj = [
          'first_name' => $request->input("first_name"),
          'last_name'=> $request->input("last_name"),
          'email' => $request->input("email"),
          'password' => Hash::make($request->input("password")),
        ];

        $validator = Validator::make($obj,[
            'first_name' => 'max:60',
            'last_name' => 'max:60',
            'email' => 'email',
            'password' => 'min:8',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(),201);
        }

        try {
            $user = User::find($id);

            if ($user == false) {
              return response()->json(['error' => 'Not found user'],201);
            }

            $user->update($obj);

            return response()->json('',204);

        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }

      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
          if (request()->header('Accept') != 'application/json') {
              return response()->json(['error' => "Request should have header 'Acept' with the value: 'aplication/json'"],403);
          }

          $user = User::find($id);

          if ($user == false) {
            return response()->json(['error' => 'Not found user'],201);
          }

          $user->delete();

          return response()->json('',204);

      } catch (\Exception $e) {
          return response()->json($e->getMessage());
      }
    }
}

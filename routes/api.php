<?php

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');

Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('logout', 'UserController@logout');
    Route::apiResource('users', 'UserController');
});
